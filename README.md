# tbanalysis_rd53b

Corryvreckan configuration files to run the testbeam analysis of the rd53b single chip modules.

## Configuration files:
- `01_create_mask`: to mask noisy and "dead" pixels; 
- `02_prealignment`;
- `03_correlations`: to check the correlations after the prealignment; 
- `04_align_telescope`: to align the telescope planes; have to be run multiple times; takes the prealignment output geometry as a starting point;
- `05_align_DUTs`: to align DUT planes (RD53B and FEI4); have to be run multiple times; takes the final telescope alignment geometry as a starting point;
- `06_analysis`: to determine efficiency; 

## Software Alignment :
Script `SoftwareAlignment.sh` can be used to perform steps from 1 to 5 at once.
Before runing the script modify:
- `path_to_data`: to correct path where runs that should be analysed are;
- `run_numbers_mask`: to list of all runs that will be used to create a mask;
- `run_number`: to run that should be used for telescope alignment;
- `momentum`: to particle energy including the unit. 

The script calls additional scripts: 
`TelescopeAlignment.sh` - to perform the telescope alignment. Parameters: `number_of_tracks`, `Tracking4D.spatial_cut_abs` and `AlignmentTrackChi2.max_track_chi2ndof` can be adjusted.

The script can be called individually as follows:
```
source TelescopeAlignment.sh data_file_name momentum
```
where momentum has to be given with the unit. 

## Analysis:

With software alignment performed one can start the hit efficiency determination. 
To determine efficiency of DUT 1 using DUT 0 as a reference: 
```
corry -c 06_analysis.conf -g plane110.role="none"
```
To determine efficiency of DUT 1 using FEI4 as a reference: 
```
corry -c 06_analysis.conf -g plane21.role="none"
```
To determine hit efficieny for multiple runs with the same condition there is additional script `Analysis.sh`.
Before runing the script modify:
- `path_to_data`: to correct path where runs that should be analysed are;
- `run_numbers`: to list of all runs to be analysed;
- `reference` to planeID of the reference DUT;
- `dut`: to planeID for which hit efficiency is determined;
- `momentum`: to particle energy including the unit. 

Use YarrConverterPlugin for eudaq1 from [here](https://github.com/arummler/eudaq), branch `v1.x-dev-rd53b`.




 


