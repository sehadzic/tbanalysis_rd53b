###############################################################################
# Usage:                                                                      #
# source ChangeParameter.sh file_input file_output string_input string_output #
###############################################################################
#!/bin/bash

sed "s/$3/$4/" $1 > $2
