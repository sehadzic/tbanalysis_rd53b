#MIMOSA26_0
mask_file = "../reconstruction/MaskCreator/plane1/mask_plane1.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = -0,0,0
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 0,0,0
spatial_resolution = 4.5um,4.5um
time_resolution = 230us
type = "mimosa26"

#FEI4
[plane21]
mask_file = "../reconstruction/MaskCreator/plane21/mask_plane21.txt"
material_budget = 0.008
number_of_pixels = 80, 336
orientation = 180deg,-0,90deg
orientation_mode = "xyz"
pixel_pitch = 250um,50um
position = 0,0,112mm
role = "dut"
spatial_resolution = 72um,14um
time_resolution = 25ns
type = "fei4"

#MIMOSA26_1
[plane2]
mask_file = "../reconstruction/MaskCreator/plane2/mask_plane2.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = -0,0,0
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 0,0,150mm
spatial_resolution = 4.5um,4.5um
time_resolution = 230us
type = "mimosa26"

#MIMOSA26_2
[plane3]
mask_file = "../reconstruction/MaskCreator/plane3/mask_plane3.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 0,0,0
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 0,0,304mm
spatial_resolution = 4.5um,4.5um
time_resolution = 230us
type = "mimosa26"

#RD53B_110
[plane110]
mask_file = "../reconstruction/MaskCreator/plane110/mask_plane110.txt"
material_budget = 0.01
number_of_pixels = 400, 192
orientation = -0,180deg,0
orientation_mode = "xyz"
pixel_pitch = 50um,50um
position = 0,0,439mm
role = "dut"
spatial_resolution = 14.434um,14.434um
time_resolution = 200ns
type = "rd53a"

#RD53B_130
[plane130]
mask_file = "../reconstruction/MaskCreator/plane130/mask_plane130.txt"
material_budget = 0.01
number_of_pixels = 400, 384
orientation = -0,180deg,-0
orientation_mode = "xyz"
pixel_pitch = 50um,50um
position = 0,0,509mm
role = "dut"
spatial_resolution = 14.434um,14.434um
time_resolution = 200ns
type = "rd53b"

#MIMOSA26_3
[plane4]
mask_file = "../reconstruction/MaskCreator/plane4/mask_plane4.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 0,0,0
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 0,0,780mm
role = "reference"
spatial_resolution = 4.5um,4.5um
time_resolution = 230us
type = "mimosa26"

#MIMOSA26_4
[plane5]
mask_file = "../reconstruction/MaskCreator/plane5/mask_plane5.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 0,0,0
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 0,0,932mm
spatial_resolution = 4.5um,4.5um
time_resolution = 230us
type = "mimosa26"

#MIMOSA26_5
[plane6]
mask_file = "../reconstruction/MaskCreator/plane6/mask_plane6.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation =0,0,0
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 0,0,1085mm
spatial_resolution = 4.5um,4.5um
time_resolution = 230us
type = "mimosa26"

