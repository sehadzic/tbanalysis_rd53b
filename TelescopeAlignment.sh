#########################################################
# Usage:                                                #
# source TelescopeAlignment.sh  data_file_name momentum #
#########################################################
#!/bin/bash
HERE=$PWD
corry -c 04_align_telescope.conf -o EventLoaderEUDAQ.file_names=$1 -o number_of_tracks=10000 -o detectors_file="$HERE/geometries/prealignment.geo" -o Tracking4D.momentum=$2 -o Tracking4D.spatial_cut_abs=100um,100um -o AlignmentTrackChi2.max_track_chi2ndof=5
corry -c 04_align_telescope.conf -o EventLoaderEUDAQ.file_names=$1 -o number_of_tracks=5000 -o Tracking4D.momentum=$2 -o Tracking4D.spatial_cut_abs=100um,100um -o AlignmentTrackChi2.max_track_chi2ndof=5
corry -c 04_align_telescope.conf -o EventLoaderEUDAQ.file_names=$1 -o number_of_tracks=2000 -o Tracking4D.momentum=$2 -o Tracking4D.spatial_cut_abs=50um,50um -o  AlignmentTrackChi2.max_track_chi2ndof=5
corry -c 04_align_telescope.conf -o EventLoaderEUDAQ.file_names=$1 -o number_of_tracks=2000 -o Tracking4D.momentum=$2 -o Tracking4D.spatial_cut_abs=50um,50um -o  AlignmentTrackChi2.max_track_chi2ndof=5
corry -c 04_align_telescope.conf -o EventLoaderEUDAQ.file_names=$1 -o number_of_tracks=2000 -o Tracking4D.momentum=$2 -o Tracking4D.spatial_cut_abs=30um,30um -o  AlignmentTrackChi2.max_track_chi2ndof=3
corry -c TelescopeTracks.conf -o EventLoaderEUDAQ.file_names=$1 -o Tracking4D.momentum=$2
