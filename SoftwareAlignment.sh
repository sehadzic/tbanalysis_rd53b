########################################
# Usage:                               #
# source SoftwareAlignment.sh          #
########################################
#!/bin/bash
HERE=$PWD

#path to the folder with the data
path_to_data="/eos/atlas/atlascerngroupdisk/pixel-upgrade/itk/BeamTest/cern_2022_may_itk/native/"
#number of the run to use for the telescope alignment and alignment of the modules outside of the cold box
run_number="1238"
#list of all runs in a batch to create a mask file
run_numbers_mask=(1238)
#data set that will be used for the telescope alignment and alignment of the modules outside of the cold box
data_file_name="${path_to_data}run00${run_number}.raw"
#particle energy
momentum="120.0GeV"

data_all_files=""
for i in ${run_numbers_mask[@]}; do
  data_all_files+="${path_to_data}run00${i}.raw,"
done

data=${data_all_files::${#data_all_files}-1}

echo "---- CREATING A MASK ----"
source AddMaskFiles.sh mask
corry -c 01_create_mask.conf -o EventLoaderEUDAQ.file_names=$data
source AddMaskFiles.sh unmask

echo "---- PREALIGNMENT FOR RUN ${i} ----"
corry -c 02_prealignment.conf -o EventLoaderEUDAQ.file_names=$data_file_name

echo "---- CHECK CORRELATIONS FOR RUN ${i} ----"
corry -c 03_correlations.conf -o EventLoaderEUDAQ.file_names=$data_file_name

echo "---- TELESCOPE ALIGNMENT FOR RUN ${i} ----"
source TelescopeAlignment.sh $data_file_name $momentum

echo "---- DUT ALIGNMENT FOR RUN ${i} ----"
source DUTAlignment.sh
