########################################
# Usage:                               #
# source AddMaskFiles.sh [mask, unmask] #
########################################
#!/bin/bash

if [ "$1" == "mask" ]; then
    echo "MASKING"
    source ChangeParameter.sh geometries/start.geo g.geo mask_file \#mask_file
elif [ "$1" == "unmask" ]; then
    echo "UNMASKING"
    source ChangeParameter.sh geometries/start.geo g.geo \#mask_file mask_file
else
    echo "Option $1 not recognized. Valid options are -mask- or -unmask-"
fi

mv g.geo geometries/start.geo
