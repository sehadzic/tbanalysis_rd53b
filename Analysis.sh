###############################
# Usage:                      #
# source Analysis.sh          #
###############################
#!/bin/bash
HERE=$PWD

#path to the folder with the data
path_to_data="/eos/atlas/atlascerngroupdisk/pixel-upgrade/itk/BeamTest/cern_2022_may_itk/native/"
#run numbers to be analysed
run_numbers=(1238)

#DUT reference
reference="plane110"
#efficiency measurement
dut="plane130"
#particle energy
momentum="120.0GeV"

for i in ${run_numbers[@]}; do
  data_file_name="${path_to_data}run00${i}.raw"
  echo "---- HIT EFFICIENCY FOR RUN ${i} DUT 1 ----"
  corry -c 06_analysis.conf -g ${reference}.role="none" -o EventLoaderEUDAQ.file_names=$data_file_name -o detectors_file="geometries/alignment_final.geo" -o histogram_file="run00${i}_REF${reference}_DUT${dut}.root" -o Tracking4D.momentum=$momentum 
done


