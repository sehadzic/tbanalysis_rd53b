#############################
# Usage:                    #
# source DUTAlignment.sh    #
#############################
HERE=$PWD
corry -c 05_align_DUTs.conf -o detectors_file="$HERE/geometries/alignment_tel.geo" -o DUTAssociation.spatial_cut_rel=15
corry -c 05_align_DUTs.conf -o DUTAssociation.spatial_cut_rel=10
corry -c 05_align_DUTs.conf -o DUTAssociation.spatial_cut_rel=5
corry -c 05_align_DUTs.conf -o DUTAssociation.spatial_cut_rel=3
